"use strict";

const input = document.querySelector(".input");
const label = document.querySelector(".label");


input.style.border = "2px solid black";

input.addEventListener("blur",()=>{
    if(input.value<0){
        const p = document.createElement("p");
        p.textContent = "Please enter correct price";
        label.after(p);
        input.style.borderColor = "red";
    }else{
        input.style.border = "2px solid black";
        input.style.backgroundColor = "green";
        const div = document.createElement("div");
        const span = document.createElement("span");
        const btn = document.createElement("button");
        span.textContent  = `Поточна ціна: ${input.value}.`
        btn.textContent = "X";
        label.before(div);
        div.append(span);
        div.append(btn);
        btn.addEventListener("click",()=>{
            div.remove();
            input.value="";
            input.style.backgroundColor = "white";
        })
    }
})

input.addEventListener("focus",(e)=>{
    input.style.outline = "0";
    input.style.borderColor = "green";
})